import React, { Component } from "react";

import Fila from "./Fila";

import "./calculadora.css";

export default class Calculadora extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pantalla: 0,
      operation: "",
      isOperatorReady: false
    };
    this.clicado = this.clicado.bind(this);
  }

  clicado(tecla) {
    const numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    const ops = ["*", "+", "-", "/"];
    let actualNumber = tecla.target.id;
    let pantalla = this.state.pantalla;

    if (actualNumber === "C") {
      this.setState({
        //borramos el contenido de la pantalla
        pantalla: 0,
        isOperatorReady: false
      });
    } else if (
      numbers.indexOf(actualNumber > -1) &&
      numbers.includes(actualNumber * 1)
    ) {
      this.setState({
        // concatenar tecla al contenido de la pantalla actual
        pantalla: pantalla + actualNumber
      });
    } else if (ops.indexOf(actualNumber > -1) && ops.includes(actualNumber)) {
      if (!this.state.isOperatorReady) {
        //simbolo operacion
        this.setState({
          pantalla: pantalla + actualNumber,
          operation: tecla.target.id,
          isOperatorReady: true
        });
      }
      console.log(actualNumber);
      console.log(this.state.operation);
    } else {
      let pantallaFinal = this.state.pantalla;
      let operator = this.state.operation;
      let resultado;

      console.log(pantallaFinal);

      switch (this.state.operation) {
        case "+":
          resultado =
            parseInt(pantallaFinal.split(operator)[0]) +
            parseInt(pantallaFinal.split(operator)[1]);
          break;
        case "-":
          resultado =
            parseInt(pantallaFinal.split(operator)[0]) -
            parseInt(pantallaFinal.split(operator)[1]);
          break;
        case "*":
          resultado =
            parseInt(pantallaFinal.split(operator)[0]) *
            parseInt(pantallaFinal.split(operator)[1]);
          break;
        case "/":
          resultado =
            parseInt(pantallaFinal.split(operator)[0]) /
            parseInt(pantallaFinal.split(operator)[1]);
          break;

        default:
          break;
      }
      console.log(resultado);
      this.setState({
        pantalla: resultado,
        isOperatorReady: false
      });
    }
  }

  render() {
    return (
      <div>
        <div className="pantalla">
          <div className="resultado">{this.state.pantalla}</div>
        </div>
        <div>
          <Fila teclas={[1, 2, 3, "*"]} onClick={this.clicado} />
          <Fila teclas={[4, 5, 6, "/"]} onClick={this.clicado} />
          <Fila teclas={[7, 8, 9, "-"]} onClick={this.clicado} />
          <Fila teclas={[0, "C", "=", "+"]} onClick={this.clicado} />
        </div>
      </div>
    );
  }
}
