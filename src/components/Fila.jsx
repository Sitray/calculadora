import React, { Component } from "react";

import "./fila.css";

export default class Fila extends Component {
  constructor(props) {
    super(props);
    this.handleOnClick = this.handleOnClick.bind(this);
  }

  handleOnClick(tecla) {
    this.props.onClick(tecla);
  }

  render() {
    let botones = this.props.teclas.map(el => (
      <div className="tecla" key={el} onClick={this.handleOnClick} id={el}>
        {el}
      </div>
    ));

    return (
      <div className="teclado">
        <div className="fila">{botones}</div>
      </div>
    );
  }
}
